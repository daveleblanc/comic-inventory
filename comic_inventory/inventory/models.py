from __future__ import unicode_literals

from django.db import models
from customer.models import Customer
# Create your models here.


class Publisher(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        ordering = ['name']


class Grade(models.Model):
    value = models.CharField(max_length=30)

    def __unicode__(self):
        return self.value

    def __str__(self):
        return self.__unicode__()

    class Meta:
        ordering = ['value']


class Collection(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        ordering = ['name']


class Item(models.Model):
    publisher = models.ForeignKey(Publisher)
    number = models.CharField(max_length=10)
    title = models.CharField(max_length=80)
    notes = models.CharField(max_length=255, blank=True)
    nm_price = models.DecimalField(max_digits=10, decimal_places=2, default="0")
    price = models.DecimalField(max_digits=10, decimal_places=2, default="0")
    stock = models.CharField(max_length=10, default="0")
    grade = models.ForeignKey(Grade, null=True, blank=True)
    grade_notes = models.CharField(max_length=80, blank=True)
    back = models.CharField(max_length=10, blank=True)
    want_list = models.ForeignKey(Customer, blank=True, null=True)
    collection = models.ForeignKey(Collection, blank=True, null=True, on_delete=models.SET_NULL,)

    class Meta:
        ordering = ['publisher', 'title', 'number']

    def get_number(self):
        try:
            float(self.number)
        except ValueError:
            return self.number
        else:
            return "#%s" % self.number.zfill(3)
    get_number.short_description = "Number"

    def save(self, *args, **kwargs):
        import re
        if re.match(r'\d+-\d+$', self.number):
            start, end = self.number.split('-')
            for n in range(int(start), int(end) + 1):
                self.pk = None
                self.number = str(n)
                self.save()
        else:
            super(Item, self).save(*args, **kwargs)

    def __unicode__(self):
        try:
            float(self.number)
        except ValueError:
            number = self.number
        else:
            number = "#%s" % self.number.zfill(3)
        return "%s:%s %s" % (self.publisher, self.title, number)

    def __str__(self):
        return self.__unicode__()


class Stock(Item):

    class Meta:
        proxy = True
