import operator

from django.conf.urls import url
from django.contrib import admin
from django.db.models import Q
from django.template.response import TemplateResponse

from .forms import StockForm
from .models import Publisher, Item, Grade, Collection, Stock


# Register your models here.
@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (('publisher', 'number'), ('title', 'notes'))
        }),
        ('Pricing', {
            'fields': (('grade', 'nm_price', 'price'), 'grade_notes')
        }),
        ('Stock', {
            'fields': (('stock', 'back'),)
        }),
        ('Additional Info', {
            'classes': ('collapse',),
            'fields': (('want_list', 'collection'),)
        }),


    )

    list_display = ('publisher', 'get_number', 'title', 'notes', 'nm_price', 'price',
                    'stock', 'grade', 'grade_notes', 'back', 'want_list', 'collection')
    list_display_links = ('publisher', 'get_number', 'title')
    list_editable = ('nm_price', 'price', 'stock', 'grade', 'back', 'want_list', 'collection')
    list_filter = ('publisher', 'grade', 'want_list', 'collection')
    list_per_page = 40
    save_as = True
    save_on_top = True

    search_fields = ('title',)

    def get_publisher(self, obj):
        return obj.publisher
    get_publisher.short_description = 'Publisher'
    get_publisher.admin_order_field = 'publisher'

    def get_grade(self, obj):
        return obj.grade
    get_grade.short_description = 'Grade'
    get_grade.admin_order_field = 'grade'


class PublisherAdmin(admin.ModelAdmin):
    search_fields = ('name',)


class StockAdmin(admin.ModelAdmin):

    list_display = ('publisher', 'get_number', 'title', 'price', 'stock', 'grade', 'grade_notes', 'back')
    list_display_links = ('publisher', 'get_number', 'title')

    def get_urls(self):
        urls = super(StockAdmin, self).get_urls()
        my_urls = [
            url(r'^$', self.floor_stock, name="floor_stock"),
            url(r'^clear_stock/$', self.clear_stock, name="clear_stock"),

        ]
        return my_urls + urls

    def floor_stock(self, request):
        items = None
        form = None
        if request.method == "GET":
            form = StockForm()

        elif request.method == "POST":
            posted_form = StockForm(request.POST)

            # check whether it's valid:
            if posted_form.is_valid():
                data = posted_form.cleaned_data
                title = data['title'].split(" ")
                number = data['number']
                items = Item.objects.filter(stock__gt=0)
                if number:
                    items = items.filter(number=number)
                items = items.filter(reduce(operator.and_, (Q(title__contains=x) for x in title)))

        # ...
        context = dict(
            # Include common variables for rendering the admin template.
            self.admin_site.each_context(request),
            form=form,
            items=items

        )
        return TemplateResponse(request, "floor_stock.html", context)

    def clear_stock(self, request):

        stock_ids = request.POST.getlist('stock')

        stock = Item.objects.filter(pk__in=stock_ids)

        for item in stock:
            item.stock = 0
            item.grade = None
            item.price = 0
            item.save()

        context = dict(
            # Include common variables for rendering the admin template.
            self.admin_site.each_context(request),
            stock=stock

        )

        return TemplateResponse(request, "clear_stock.html", context)

admin.site.register(Stock, StockAdmin)

admin.site.register(Collection)
admin.site.register(Grade)
admin.site.register(Publisher, PublisherAdmin)
