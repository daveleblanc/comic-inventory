# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-14 19:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_auto_20160914_1921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='inventory.Collection'),
        ),
    ]
