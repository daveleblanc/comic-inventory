'''
Created on Sep 30, 2016

@author: dave
'''
from django import forms


class StockForm(forms.Form):
    number = forms.CharField(max_length=20, label="issue number", required=False)
    title = forms.CharField(max_length=255, label="title")
