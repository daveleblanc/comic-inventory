'''
Created on Sep 12, 2016

@author: dave

Import script for CSV with the following columns. 
Columns MUST be in this order actual header values irrelevant and header row is optional. 
Default value for header parameter is True

PUB    NUM    TITLE    NOTES    NM PRICE    PRICE    STOCK     GRADE    BACK    BOX    WANT LIST
'''

import csv
from inventory.models import Item, Publisher, Grade
from customer.models import Customer


def create_inventory(row):
    pub, num, title, notes, nm_price, price, stock, gr, back, box, cust = row
    publisher, _ = Publisher.objects.get_or_create(name=pub)
    grade, _ = Grade.objects.get_or_create(value=gr)
    want_list = None
    if cust:
        want_list, _ = Customer.objects.get_or_create(name=cust)

    item = Item(publisher=publisher,
                number=num,
                title=title,
                notes=notes,
                stock=stock,
                grade=grade,
                back=back,
                box=box,
                want_list=want_list)
    if nm_price:
        item.nm_price = nm_price.replace('$', '')
    if price:
        item.price = price.replace('$', '')
    try:
        item.save()
    except:
        print row


def inventory_import(file_name, header=None):
    if header == None:
        header = True

    with open(file_name, 'rb') as csvfile:
        inventory_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(inventory_reader, None)  # skip the headers
        for row in inventory_reader:
            create_inventory(row)
